import param
import scrolling
import descriptors as dc

import numpy as np
import librosa.display
import matplotlib.pyplot as plt

esc = False
while not esc:
    # read current sound file, perform transform
    sound, param.SAMPLING_RATE = librosa.load(param.sound_files[param.fileindex])
    S_db = librosa.amplitude_to_db(np.abs(librosa.stft(sound, n_fft=param.N_FFT, hop_length=param.HOP_LEN,
                                                       win_length=param.FRAME_LEN, window=param.WINDOW_TYPE)),
                                   ref=np.max)

    # get descriptors
    rms = dc.root_mean_square(sound)
    zero_crossing_rate = dc.zero_crossing_rate(sound)
    spectral_centroid = dc.spectral_centroid(sound)
    spectral_spread = dc.spectral_spread(sound)
    spectral_variation = dc.spectral_variation(sound)
    spectral_flatness = dc.spectral_flatness(sound)

    time = librosa.times_like(spectral_centroid, sr=param.SAMPLING_RATE, hop_length=param.HOP_LEN, n_fft=param.N_FFT)

    # plots
    orig_fig, orig_axs = plt.subplots(2)
    orig_fig.set_size_inches(25 / 2.54, 12 / 2.54)
    orig_axs[0].grid()
    orig_axs[0].set_title('Original sound waveform (' + scrolling.get_filename() + ')')
    librosa.display.waveplot(sound, sr=param.SAMPLING_RATE, x_axis='time', ax=orig_axs[0])
    orig_axs[1].grid()
    orig_axs[1].set_title('Original sound spectrogram (' + scrolling.get_filename() + ')')
    spectrogram = librosa.display.specshow(S_db, x_axis='time', y_axis='log', sr=param.SAMPLING_RATE,
                                           hop_length=param.HOP_LEN, ax=orig_axs[1])
    plt.tight_layout()

    fig, axs = plt.subplots(3, 2)
    fig.set_size_inches(32 / 2.54, 15 / 2.54)
    fig.suptitle('Instantaneous descriptors (' + scrolling.get_filename() + ')')

    axs[0][0].set_title('RMS')
    axs[0][0].plot(time, rms)
    axs[0][0].grid()

    axs[1][0].set_title('Zero crossing rate')
    axs[1][0].plot(time, zero_crossing_rate)
    axs[1][0].grid()

    axs[2][0].set_title('Spectral centroid')
    axs[2][0].plot(time, spectral_centroid)
    axs[2][0].grid()

    axs[0][1].set_title('Spectral spread')
    axs[0][1].plot(time, spectral_spread)
    axs[0][1].grid()

    axs[1][1].set_title('Spectral variation')
    axs[1][1].plot(time, spectral_variation)
    axs[1][1].grid()

    axs[2][1].set_title('Spectral flatness')
    axs[2][1].plot(time, spectral_flatness)
    axs[2][1].grid()

    plt.tight_layout()
    plt.show()

    esc = scrolling.scroll_end()
    print()
