import numpy as np
import librosa
import param

# all functions take the sound in its original waveform (and perform any transforms internally)


def root_mean_square(y, frame_length=param.FRAME_LEN, hop_length=param.HOP_LEN):
    return librosa.feature.rms(y=y, frame_length=frame_length, hop_length=hop_length)[0]


def zero_crossing_rate(y, frame_length=param.FRAME_LEN, hop_length=param.HOP_LEN):
    return librosa.feature.zero_crossing_rate(y=y, frame_length=frame_length, hop_length=hop_length)[0]


def log_attack_time(y, sr=param.SAMPLING_RATE, fram_len=param.FRAME_LEN, hop_len=param.HOP_LEN):
    """
    computes the log-attack time from the waveform (time from 20% to 90%)
      Args:
        y: waveform
        f_s: sample rate of audio data

      Returns:
        lat (in secs)
    """
    eps = np.finfo(float).eps

    def find_nearest_arg(array, value):
        array = np.asarray(np.squeeze(array))
        idx = (np.abs(array - value)).argmin()
        return idx

    rms = librosa.feature.rms(y=y, frame_length=fram_len, hop_length=hop_len)[0]
    e20 = 0.2*np.max(rms)
    e90 = 0.9*np.max(rms)
    e90_pos = find_nearest_arg(rms, e90)
    e20_pos = find_nearest_arg(rms[0:e90_pos], e20)
    e20_time = e20_pos * hop_len/sr
    e90_time = e90_pos * hop_len/sr
    lat = np.log(e90_time - e20_time + eps)
    return lat


def temporal_centroid():
    return


def effective_duration():
    return


def spectral_spread(y, sr=param.SAMPLING_RATE, n_fft=param.N_FFT, hop_length=param.HOP_LEN, window=param.WINDOW_TYPE):
    return librosa.feature.spectral_bandwidth(y=y, sr=sr, n_fft=n_fft, hop_length=hop_length, window=window)[0]


def spectral_centroid(y, sr=param.SAMPLING_RATE, n_fft=param.N_FFT, hop_length=param.HOP_LEN, window=param.WINDOW_TYPE):
    return librosa.feature.spectral_centroid(y=y, sr=sr, n_fft=n_fft, hop_length=hop_length, window=window)[0]


def spectral_variation(y, sr=param.SAMPLING_RATE, n_fft=param.N_FFT,
                       hop_length=param.HOP_LEN, frame_len=param.FRAME_LEN, window=param.WINDOW_TYPE):
    """
    adapted from https://www.audiocontentanalysis.org/code/audio-features/spectral-flux-2/
    computes the spectral flux from the magnitude spectrum

      Args:
        y: waveform
        f_s: sample rate of audio data

      Returns:
        vsf: spectral flux
    """
    X = np.abs(librosa.stft(y, n_fft=n_fft, hop_length=hop_length, win_length=frame_len, window=window))
    isSpectrum = X.ndim == 1
    if isSpectrum:
        X = np.expand_dims(X, axis=1)
    # difference spectrum (set first diff to zero)
    X = np.c_[X[:, 0], X]
    afDeltaX = np.diff(X, 1, axis=1)
    # flux
    vsf = np.sqrt((afDeltaX**2).sum(axis=0)) / X.shape[0]
    return np.squeeze(vsf) if isSpectrum else vsf


def spectral_flatness(y, n_fft=param.N_FFT, hop_length=param.HOP_LEN, window=param.WINDOW_TYPE):
    return librosa.feature.spectral_flatness(y=y, n_fft=n_fft, hop_length=hop_length, window=window)[0]


# mean, std, min, max, orig, norm
def modify(descriptor, mod_string, sound):
    if mod_string == 'mean':
        return np.mean(descriptor)
    elif mod_string == 'std':
        return np.std(descriptor)
    elif mod_string == 'min':
        return np.min(descriptor)
    elif mod_string == 'max':
        return np.max(descriptor)
    elif mod_string == 'norm':
        return descriptor / librosa.get_duration(sound, sr=param.SAMPLING_RATE, n_fft=param.N_FFT,
                                                 hop_length=param.HOP_LEN)
    else:
        return descriptor
