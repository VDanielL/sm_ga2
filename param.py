import scrolling
import pandas as pd

# specify these
WINDOW_SIZE = 60  # ms
HOP_SIZE = 10  # ms
SAMPLING_RATE = 22050  # Hz
N_FFT = 2048
WINDOW_TYPE = 'hann'

INSTRUMENTS = ['gui', 'flt', 'acc', 'vln']
DIRECTORY = 'InstrumentalSounds/Original'
CONTINUOUS_DISPLAY = False
TASK_THREE_PLOT = 'file'
# format: <y_mod> <y_func> by <x_mod> <x_func>
# or: file
# options for instantaneous descriptors modifiers: mean, std, min, max
# options for global descriptors modifiers: orig, norm
# e.g.: 'orig log_attack_time by std spectral_centroid'
TASK_THREE_OPTIONS_FILE = 'task_three_options.xlsx'
TASK_THREE_COLOURS = ['red', 'orange', 'blue', 'green']

# parameter conversions
FRAME_LEN = round(WINDOW_SIZE * SAMPLING_RATE / 1e3)
HOP_LEN = round(HOP_SIZE * SAMPLING_RATE / 1e3)

# input sounds into array
sound_files = list()
scrolling.scroll_read(sound_files, DIRECTORY, INSTRUMENTS)

fileindex = 0

# read task_three_plot_options from file
if TASK_THREE_PLOT == 'file':
    task_three_options = pd.read_excel(TASK_THREE_OPTIONS_FILE)

else:
    # obtain x- and y-axis function names and modifiers
    three_y_axis_mod = TASK_THREE_PLOT.split(' ')[0]
    three_y_axis_func = TASK_THREE_PLOT.split(' ')[1]
    three_x_axis_mod = TASK_THREE_PLOT.split(' ')[3]
    three_x_axis_func = TASK_THREE_PLOT.split(' ')[4]
