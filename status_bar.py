"""
STATUS BAR FUNCTIONS

This file contains the function necessary for drawing the status bar.
"""

import sys

bar_length = 70
fill_character = '█'
empty_character = ' '


def draw_status_bar(progress, max):
    percentage = round(100 * progress / max, 2)
    block_number = int(round(progress / max * bar_length))
    to_print = '\r|{0}| {1}/{2} ({3} %)'.format(
        block_number * fill_character + empty_character * (bar_length - block_number),
        progress+1, max, percentage)
    sys.stdout.write(to_print)
    sys.stdout.flush()
