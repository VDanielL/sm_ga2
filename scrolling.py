import glob
from msvcrt import getche
import librosa
import param


# (re-)read the entire folder for all datasets matching instrument criteria
def scroll_read(sounds, directory, instruments):
    sounds.clear()
    for instr in instruments:
        files = glob.glob(directory + '/' + instr + '*.wav', recursive=True)
        for file in files:
            sounds.append(file)


# scrolling update at the end of the loop
def scroll_end():

    scroll_read(param.sound_files, param.DIRECTORY, param.INSTRUMENTS)

    if param.CONTINUOUS_DISPLAY:
        return False, param.fileindex + 1 if param.fileindex < len(param.sound_files) - 1 else 0

    letterinfo = 'n: next, p: previous, #[0-9]: jump # forwards, -#[0-9]: jump # backwards, s: specific number, ' \
                 'e: escape, (default): reload '
    print(letterinfo)
    answer = getche().decode(errors='replace')

    if answer == 'e':
        return True
    elif answer == 'n':
        param.fileindex = param.fileindex + 1 if param.fileindex < len(param.sound_files) - 1 else 0
        return False
    elif answer == 'p':
        param.fileindex = param.fileindex - 1 if param.fileindex > 0 else len(param.sound_files) - 1
        return False
    elif '0' <= answer <= '9':
        param.fileindex = param.fileindex + int(answer) if param.fileindex < \
                                                             len(param.sound_files) - int(answer) else \
            (int(answer) - (len(param.sound_files) - param.fileindex)) % len(param.sound_files)
        return False
    elif answer == '-':
        answer = getche().decode(errors='replace')
        if '0' <= answer <= '9':
            param.fileindex = param.fileindex - int(answer) if param.fileindex >= int(answer) else \
                len(param.sound_files) - ((int(answer) - param.fileindex) % len(param.sound_files))
            return False
        else:
            print('\n')
            return False
    elif answer == 's':
        param.fileindex = int(input('pecify number (min 1, max {}): '.format(len(param.sound_files)))) - 1
        return False
    else:
        print('\n')
        return False


def get_filename():
    return param.sound_files[param.fileindex].split('\\')[-1]
