import param
import scrolling
import descriptors as dc
import status_bar as sb

import librosa.display
import matplotlib.pyplot as plt

def plot_x_y_values(x_func, x_mod, y_func, y_mod, axs):
    files = list()
    x = list()
    y = list()
    # obtain x- and y-axis values for all files previously specified for all instruments and plot them
    for i, instr in enumerate(param.INSTRUMENTS):
        x.clear()
        y.clear()

        scrolling.scroll_read(files, param.DIRECTORY, [instr])
        for file in files:
            sound, param.SAMPLING_RATE = librosa.load(file)
            x_tmp = eval('dc.' + x_func + '(sound)')
            x.append(dc.modify(x_tmp, x_mod, sound))
            y_tmp = eval('dc.' + y_func + '(sound)')
            y.append(dc.modify(y_tmp, y_mod, sound))

        axs.plot(x, y, color=param.TASK_THREE_COLOURS[i % len(param.TASK_THREE_COLOURS)],
                marker='o', linestyle='', label=instr)
    return


if param.TASK_THREE_PLOT == 'file':
    length = len(param.task_three_options.index)
    fig, ax = plt.subplots(1)
    for i, option in param.task_three_options.iterrows():
        title = option['three_y_axis_mod'] + ' ' + option['three_y_axis_func'] + ' by ' + \
                option['three_x_axis_mod'] + ' ' + option['three_x_axis_func']
        plot_x_y_values(option['three_x_axis_func'], option['three_x_axis_mod'],
                        option['three_y_axis_func'], option['three_y_axis_mod'], ax)
        fig.suptitle(title)
        ax.set_xlabel(option['three_x_axis_mod'] + ' ' + option['three_x_axis_func'])
        ax.set_ylabel(option['three_y_axis_mod'] + ' ' + option['three_y_axis_func'])
        ax.legend()
        plt.tight_layout()
        plt.savefig('task3_figures/' + title + '.png')
        ax.cla()
        sb.draw_status_bar(i, length)

else:
    fig, ax = plt.subplots(1)
    plot_x_y_values(param.three_x_axis_func, param.three_x_axis_mod,
                    param.three_y_axis_func, param.three_y_axis_mod, ax)
    fig.suptitle(param.TASK_THREE_PLOT)
    ax.set_xlabel(param.three_x_axis_mod + ' ' + param.three_x_axis_func)
    ax.set_ylabel(param.three_y_axis_mod + ' ' + param.three_y_axis_func)
    ax.legend()
    plt.tight_layout()
    plt.show()
